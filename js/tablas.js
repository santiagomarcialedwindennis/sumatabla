
function numeros(tabla){
  var celdas=tabla.getElementsByTagName("td");
  var tmp=[];
  for(var i=0;i<celdas.length;i++){
    if(celdas[i].getAttribute('name')=="numeros")
    tmp.push(Number(celdas[i].textContent));
  }
  return tmp;
}

function resultado(arreglo){
  var cajaResultado = document.getElementById("resultado");
  var acumulador=0;
  for(var i=0;i<arreglo.length;i++)
    acumulador+=arreglo[i];
  
  document.getElementById("resultado").textContent=acumulador;
  
}
